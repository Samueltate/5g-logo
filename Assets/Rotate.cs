﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    public float speed = 1;

    public float rotateRange = 45.0f;

    public bool rotateOtherway = false;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //this.transform.Rotate(speed * Vector3.forward * Time.deltaTime);

        
        

        if (rotateOtherway)
        {
            this.transform.Rotate(Vector3.forward * speed * Mathf.Sin(Time.deltaTime));
        }
        else
        {
            this.transform.Rotate(-Vector3.forward * speed * Mathf.Sin(Time.deltaTime));
        }

        if (Mathf.Abs(this.transform.rotation.eulerAngles.z) >= rotateRange)
        {
            rotateOtherway = !rotateOtherway;
        }
    }
}
